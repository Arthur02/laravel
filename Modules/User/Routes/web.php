<?php

use Illuminate\Support\Facades\Route;
use Modules\User\Http\Controllers\RegistrationController;
use Modules\User\Http\Controllers\LoginController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['module' => 'User', 'middleware' => ['web'], 'namespace' => 'App\Modules\User\Controllers'], function() {
    // show login page
    Route::get('/', [LoginController::class, 'show']);
    Route::get('/login', [LoginController::class, 'show'])->name('login');
    // login to acc
    Route::post('/login', [LoginController::class, 'authenticate']);
    // logout function
    Route::get('/logout', [LoginController::class, 'logout']);

    // show register page
    Route::get('/register', [RegistrationController::class, 'show'])->name('register')->middleware('guest');
    // registration function
    Route::post('/register', [RegistrationController::class, 'register']);

    // Log out User
    Route::get('/logout', [LoginController::class, 'logout']);

});

// Route::middleware(['auth'])->group(function () {
// });
