<?php

namespace Modules\User\Http\Controllers;

use App\Traits\RegisterUser;
use App\Http\Requests\RegistrationRequest;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use Modules\helper;

class RegistrationController extends Controller
{
    use RegisterUser;

    public function __construct(){
        $this->helper = new helper();
    }
    
    public function show()
    {
        return view('user::sign_up');
    }

    public function register(Request $requestFields)
    {
        $response = $this->helper->ApiRegisterCall($requestFields, 'register');
        $data = $response->json();
        if($response->status() === 200){
            Session::flash('success', "you have successfuly created user {$data['email']}");
            return  Redirect::to('login');
        }
        else{
            $errors = $data;
            Session::flash('apiError', $errors);
            return back();
        }
    }
}
