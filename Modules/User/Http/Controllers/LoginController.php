<?php

namespace Modules\User\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\Request;
use Modules\User\Providers\UserServiceProvider;
use Modules\helper;

class LoginController extends Controller
{
    public function __construct(){
        $this->helper = new helper();
    }
    public function show()
    {
        $helper = new helper();
        return view("user::login");
    }

    public function authenticate(Request $requestFields)
    {
        $response = $this->helper->ApiLoginCall($requestFields, 'login');
        $data = $response->json();
        if($response->status() === 200){
            Session::put('user_id', $response['id']);
            if ($response['role'] === "admin") {
                return  Redirect::to('admin')->with('user', $data);
            } else{
                return  Redirect::to('dashboard')->with('user', $data);
            }
        }
        else{
            $errors = $data;
            Session::flash('apiError', $errors);
            return back();
        }
    }

    public function logout()
    {
        Session::flush();
        return Redirect::to('login');
    }
}
