@extends('user::Layouts._master')
@section('title')
    <title>{{env('WEBSITE_TITLE')}} | SignUp</title>
@endsection
@section('main-content')
    <!--begin::Signup-->
    <div class="login-form pt-2">
        <!--begin::Form-->
        <form class="form" novalidate="novalidate" method="post" action="/register">
            @csrf
            <!--begin::Title-->
            <div class="text-center pb-8">
                <h2 class="font-weight-bolder font-size-h2 font-size-h1-lg">Sign Up</h2>
                <p class="text-muted font-weight-bold font-size-h4">Enter your details to create your account</p>
            </div>
            <!--end::Title-->

            <!--begin::Form group-->
            <div class="form-group">
                <div class="input-icon">
                    <input class="form-control form-control-solid h-auto py-7 rounded-lg font-size-h6" type="text" placeholder="Fullname" name="name" autocomplete="off"/>
                    <span><i class="far fa-user"></i></span>
                </div>
            </div>
            <!--end::Form group-->

            <!--begin::Form group-->
            <div class="form-group">
                <div class="input-icon">
                    <input class="form-control form-control-solid h-auto py-7 rounded-lg font-size-h6" type="text" placeholder="Username" name="username" autocomplete="off"/>
                    <span><i class="far fa-user"></i></span>
                </div>
            </div>
            <!--end::Form group-->

            <!--begin::Form group-->
            <div class="form-group">
                <div class="input-icon">
                    <input class="form-control form-control-solid h-auto py-7 rounded-lg font-size-h6" type="email" placeholder="Email" name="email" autocomplete="off"/>
                    <span><i class="far fa-envelope-open"></i></span>
                </div>
            </div>
            <!--end::Form group-->

            <!--begin::Form group-->
            <div class="form-group">
                <div class="input-icon">
                    <input class="form-control form-control-solid h-auto py-7 rounded-lg font-size-h6" type="password" placeholder="Password" name="password" autocomplete="off"/>
                    <span><i class="fas fa-unlock"></i></span>
                </div>
            </div>
            <!--end::Form group-->

            <!--begin::Form group-->
            <div class="form-group">
                <div class="input-icon">
                    <input class="form-control form-control-solid h-auto py-7 rounded-lg font-size-h6" type="password" placeholder="Confirm password" name="cpassword" autocomplete="off"/>
                    <span><i class="fas fa-unlock"></i></span>
                </div>
            </div>
            <!--end::Form group-->

            <!--begin::Form group-->
            <div class="form-group">
                <label class="checkbox mb-0">
                    <input type="checkbox" name="agree"/>
                    <span class="mr-2"></span>
                    I Agree the &nbsp;<a href="#" class="text-primary"> terms and conditions</a>.
                </label>
            </div>
            <!--end::Form group-->

            <!--begin::Form group-->
            <div class="form-group d-flex flex-wrap flex-center">
                <button style="display: none" id="clickMe"></button>
                <a id="" class="btn font-weight-bolder font-size-h6 px-8 py-4 my-3 mx-4" onclick="clickMe.click()">Submit</a>
                <button type="button" id="Sb_login_signup_cancel" class="btn font-weight-bolder font-size-h6 px-8 py-4 my-3 mx-4">Cancel</button>
            </div>
            <!--end::Form group-->
        </form>
        <!--end::Form-->
    </div>

    <!--end::Signup-->
@endsection
@section('footer-content')
    <!-- begin:Signin -->
    <div class="text-center pt-2">
        <span class="font-weight-bold font-size-h4">Already have an account? <a href="/login" class="text-primary font-weight-bolder" id="">Sign In</a></span>
    </div>
    <!-- end:Signin -->
@endsection
