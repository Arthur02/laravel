@extends('user::Layouts._master')
@section('title')
    <title>{{env('WEBSITE_TITLE')}} | Login</title>
@endsection
@section('main-content')
    <!--begin::Signin-->
    <div class="login-form py-11">
        <!--begin::Form-->
        <form class="form" method="post" action="/login" id="form">
            @csrf
            <!--begin::Title-->
            <div class="text-center pb-8">
                <h2 class="font-weight-bolder font-size-h2 font-size-h1-lg">Sign In</h2>
                <span class="font-weight-bold font-size-h4">Or <a href="/register" class="text-primary font-weight-bolder" id="Sb_login_signup">Create An Account</a></span>
            </div>
            <!--end::Title-->

            <!--begin::Form group-->
            <div class="form-group">
                <div class="input-icon">
                    <input class="form-control form-control-solid h-auto py-7 rounded-lg font-size-h6" type="text" name="email" autocomplete="off" placeholder="User Name"/>
                    <span><i class="far fa-user"></i></span>
                </div>
            </div>
            <!--end::Form group-->

            <!--begin::Form group-->
            <div class="form-group">
                <div class="input-icon">
                    <input class="form-control form-control-solid h-auto py-7 rounded-lg font-size-h6" type="password" name="password" autocomplete="off" placeholder="Password"/>
                    <span><i class="fas fa-unlock"></i></span>
                </div>
            </div>
            <!--end::Form group-->

            <!--begin::Form group-->
            <div class="form-group">
                <div class="d-flex justify-content-between mt-n5">
                    <label class="checkbox mb-0 pt-5">
                        <input type="checkbox" name="remember-me"/>
                        <span  class="mr-2"></span>
                        Remember Me
                    </label>
                    <a href="forgot_password.html" class="text-primary font-size-h6 font-weight-bolder text-hover-primary pt-5" id="">
                        Forgot Password ?
                    </a>
                </div>
            </div>
            <!--end::Form group-->


            <!--begin::Action-->
            <div class="text-center pt-2">
                <button style="display: none" id="clickMe"></button>
                <a id="" class="btn font-weight-bolder font-size-h6 px-8 py-4 my-3" onclick="clickMe.click()">Sign In</a>
            </div>
            <!--end::Action-->
        </form>
        <!--end::Form-->
    </div>
    <!--end::Signin-->
@endsection
