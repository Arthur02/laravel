<!DOCTYPE html>
<html lang="en" >
    <!--begin::Head-->
    <head>
        <base href="">
        <meta charset="utf-8"/>
        <title>SocioBoard | Login</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
            
        <meta name="google-site-verification" content="" />
        <meta name="description" content="Be it marketing(finding leads/customers) on Social media, or listening to customer complaints, replying to them, managing multiple social media accounts from one single dashboard, finding influencers in a particular category and reaching out to them and many more things, Socioboard products can do it." />
        <meta name="keywords" content="Social Media Management Software, Social Media Management tool, Open Source Social Media Management, Social Media Management" />
        <meta name="author" content="Socioboard Technologies">
        <meta name="designer" content="Chanchal Santra">

        <!--begin::Fonts-->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700"/>        
        <!--end::Fonts-->

        <!--begin::Global Theme Styles(used by all pages)-->
        <link href="{{asset('plugins/global/plugins.bundle.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{asset('plugins/custom/prismjs/prismjs.bundle.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{asset('css/style.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{asset('css/dark.css')}}" rel="stylesheet" type="text/css"/>

        @yield('links')
        <!--end::Global Theme Styles-->

        <!--begin::Layout Themes(used by all pages)-->
        <!--end::Layout Themes-->

        <link rel="shortcut icon" href="{{asset('media/logos/favicon.ico')}}"/>

    </head>
    <!--end::Head-->

    <!--begin::Body-->
    <body  id="Sb_body"  class="header-fixed header-mobile-fixed subheader-enabled page-loading"  >
   <!--begin::Main-->
	<!--begin::Header Mobile-->
    <div id="Sb_header_mobile" class="header-mobile header-mobile-fixed " >
        <!--begin::Logo-->
        <a href="index.html">
            <img alt="SocioBoard" src="{{asset('media/logos/sb-icon.svg')}}" style="width: 50px;" class="max-h-30px mt-5"/>
        </a>
        <!--end::Logo-->

        <!--begin::Toolbar-->
        <div class="d-flex align-items-center">
            <button class="btn p-0 burger-icon burger-icon-left ml-4" id="Sb_header_mobile_toggle">
                <span></span>
            </button>

            <button class="btn p-0 ml-2" id="Sb_header_mobile_topbar_toggle">
                <span class="svg-icon svg-icon-xl"><!--begin::Svg Icon | path:assets/media/svg/icons/General/User.svg-->
                    <i class="fas fa-user-cog"></i>
                </span>		
            </button>
        </div>
        <!--end::Toolbar-->
    </div>
<!--end::Header Mobile-->
	<div class="d-flex flex-column flex-root">
		<!--begin::Page-->
		<div class="d-flex flex-row flex-column-fluid page">
			<!--begin::Wrapper-->
			<div class="d-flex flex-column flex-row-fluid wrapper" id="Sb_wrapper">
				<!--begin::Header-->
                    <div id="Sb_header" class="header flex-column  header-fixed " >
                        <!--begin::Top-->
                        <div class="header-top">
                            <!--begin::Container-->
                            <div class=" container-fluid ">
                                <!--begin::Left-->
                                <div class="d-none d-lg-flex align-items-center mr-3">
                                    <!--begin::Logo-->
                                    <a href="index.html" class="mr-2">
                                        <img alt="SocioBoard" src="{{asset('media/logos/sb-icon.svg')}}" style="width: 50px;" class="max-h-35px mt-5"/>
                                    </a>
                                    <!--end::Logo-->
    
                                    <!--begin::Tab Navs(for desktop mode)-->
                                    <ul class="header-tabs nav align-self-end font-size-lg" role="tablist">
                                        <!--begin::Item-->
                                        <li class="nav-item">
                                            <a href="#" class="nav-link py-4 px-6 active" data-toggle="tab" data-target="#Sb_header_dashboard" role="tab">
                                                Home
                                            </a>
                                        </li>
                                        <!--end::Item-->

                                        <!--begin::Item-->
                                        <li class="nav-item mr-3">
                                            <a href="#" class="nav-link py-4 px-6" data-toggle="tab" data-target="#Sb_header_discovery" role="tab">
                                                Discovery
                                            </a>
                                        </li>
                                        <!--end::Item-->

                                        <!--begin::Item-->
                                        <li class="nav-item mr-3">
                                            <a href="#" class="nav-link py-4 px-6" data-toggle="tab" data-target="#Sb_header_reports" role="tab">
                                                Reports
                                            </a>
                                        </li>
                                        <!--end::Item-->

                                        <!--begin::Item-->
                                        <li class="nav-item mr-3">
                                            <a href="#" class="nav-link py-4 px-6" data-toggle="tab" data-target="#Sb_header_boards" role="tab">
                                                Boards
                                            </a>
                                        </li>
                                        <!--end::Item-->

                                        <!--begin::Item-->
                                        <li class="nav-item mr-3">
                                            <a href="#" class="nav-link py-4 px-6" data-toggle="tab" data-target="#Sb_header_library" role="tab">
                                                Image Library
                                            </a>
                                        </li>
                                        <!--end::Item-->
                                            
                                        <!--begin::Item-->
                                        <li class="nav-item mr-3">
                                            <a href="#" class="nav-link py-4 px-6" data-toggle="tab" data-target="#Sb_header_chat" role="tab">
                                                Chat
                                            </a>
                                        </li>
                                        <!--end::Item-->
                                    </ul>
                                    <!--begin::Tab Navs-->
                                </div>
                                <!--end::Left-->
    
                                <!--begin::Topbar-->
                                <div class="topbar mr-2">
                                    <!-- dark switch -->
                                    <div class="theme-switch-wrapper">
                                        <span class="opacity-50 font-weight-bold font-size-sm d-none d-md-inline mt-4">theme</span>
                                        <span class="switch switch-outline switch-icon switch-dark">
                                            <label class="theme-switch" for="checkbox">
                                                <input type="checkbox" id="checkbox" checked="checked" name="select"/>
                                                <span></span>
                                            </label>
                                        </span>
                                    </div>		 
                                <!--end::Search-->
    
                                   
    
                                    <!--begin::Notifications-->
                                    <div class="dropdown">
                                        <!--begin::Toggle-->
                                        <div class="topbar-item" data-toggle="dropdown" data-offset="10px,0px">
                                            <div class="btn btn-icon btn-dropdown btn-lg mr-1 pulse pulse-white">
                                                <span class="svg-icon svg-icon-xl">
                                                    <i class="fas fa-bell"></i>
                                                </span>			                        
                                                <span class="pulse-ring"></span>
                                            </div>
                                        </div>
                                        <!--end::Toggle-->
    
                                        <!--begin::Dropdown-->
                                        <div class="dropdown-menu p-0 m-0 dropdown-menu-right dropdown-menu-anim-up dropdown-menu-lg">
                                            <form>
                                                <!--begin::Header-->
                                                <div class="d-flex flex-column pt-5 bgi-size-cover bgi-no-repeat rounded-top" style="background-image: url("{{asset('media/misc/bg-1.jpg')}}")">
                                                    <!--begin::Title-->
                                                    <h4 class="d-flex flex-center rounded-top">
                                                        <span class="text-white">User Notifications</span>
                                                        <span class="btn btn-text btn-success btn-sm font-weight-bold btn-font-md ml-2">23 new</span>
                                                    </h4>
                                                    <!--end::Title-->
    
                                                    <!--begin::Tabs-->
                                                    <ul class="nav nav-bold nav-tabs nav-tabs-line nav-tabs-line-3x nav-tabs-line-active-border-success mt-3 px-8" role="tablist">
                                                        <li class="nav-item">
                                                            <a class="nav-link active show" data-toggle="tab" href="#topbar_notifications_notifications"  >All</a>
                                                        </li>
                                                        <li class="nav-item">
                                                            <a class="nav-link" data-toggle="tab" href="#topbar_notifications_publishing"  >Publishing</a>
                                                        </li>
                                                        <li class="nav-item">
                                                            <a class="nav-link" data-toggle="tab" href="#topbar_notifications_teams"  >Teams</a>
                                                        </li>
                                                    </ul>
                                                    <!--end::Tabs-->
                                                </div>
                                                <!--end::Header-->
    
                                                <!--begin::Content-->
                                                <div class="tab-content">
                                                    <!--begin::Tabpane-->
                                                    <div class="tab-pane active show p-8" id="topbar_notifications_notifications" role="tabpanel">
                                                        <!--begin::Scroll-->
                                                        <div class="scroll pr-7 mr-n7" data-scroll="true" data-height="300" data-mobile-height="200">
                                                            <!--begin::Item-->
                                                            <div class="d-flex align-items-center mb-6">
                                                                <!--begin::Symbol-->
                                                                <div class="symbol symbol-40 symbol-light-primary mr-5">
                                                                    <span class="symbol-label">
                                                                        <span class="svg-icon svg-icon-lg svg-icon-primary">
                                                                             
                                                                        </span>                    
                                                                    </span>
                                                                </div>
                                                                <!--end::Symbol-->
    
                                                                <!--begin::Text-->
                                                                <div class="d-flex flex-column font-weight-bold">
                                                                    <a href="#" class="mb-1 font-size-lg">Cool App</a>
                                                                    <span class="">Marketing campaign planning</span>
                                                                </div>
                                                                <!--end::Text-->
                                                            </div>
                                                            <!--end::Item-->
    
                                                            <!--begin::Item-->
                                                            <div class="d-flex align-items-center mb-6">
                                                                <!--begin::Symbol-->
                                                                <div class="symbol symbol-40 symbol-light-warning mr-5">
                                                                    <span class="symbol-label">
                                                                        <span class="svg-icon svg-icon-lg svg-icon-warning">
                                                                            
                                                                        </span>                    
                                                                    </span>
                                                                </div>
                                                                <!--end::Symbol-->
    
                                                                <!--begin::Text-->
                                                                <div class="d-flex flex-column font-weight-bold">
                                                                    <a href="#" class="mb-1 font-size-lg">Awesome SAAS</a>
                                                                    <span class="">Project status update meeting</span>
                                                                </div>
                                                                <!--end::Text-->
                                                            </div>
                                                            <!--end::Item-->
    
                                                        </div>
                                                        <!--end::Scroll-->
    
                                                        <!--begin::Action-->
                                                        <div class="d-flex flex-center pt-7"><a href="#" class="btn font-weight-bold text-center">See All</a></div>
                                                        <!--end::Action-->
                                                    </div>
                                                    <!--end::Tabpane-->
    
                                                    <!--begin::Tabpane-->
                                                    <div class="tab-pane" id="topbar_notifications_publishing" role="tabpanel">
                                                        <!--begin::Nav-->
                                                        <div class="navi navi-hover scroll my-4" data-scroll="true" data-height="300" data-mobile-height="200">
                                                            <!--begin::Item-->
                                                            <a href="#" class="navi-item">
                                                                <div class="navi-link">
                                                                    <div class="navi-icon mr-2">
                                                                        <i class="flaticon2-line-chart text-success"></i>
                                                                    </div>
                                                                    <div class="navi-text">
                                                                        <div class="font-weight-bold">
                                                                            New report has been received
                                                                        </div>
                                                                        <div class="text-muted">
                                                                            23 hrs ago
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </a>
                                                            <!--end::Item-->
    
                                                            <!--begin::Item-->
                                                            <a href="#" class="navi-item">
                                                                <div class="navi-link">
                                                                    <div class="navi-icon mr-2">
                                                                        <i class="flaticon2-paper-plane text-danger"></i>
                                                                    </div>
                                                                    <div class="navi-text">
                                                                        <div class="font-weight-bold">
                                                                            Finance report has been generated
                                                                        </div>
                                                                        <div class="text-muted">
                                                                            25 hrs ago
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </a>
                                                            <!--end::Item-->
    
                                                        </div>
                                                        <!--end::Nav-->
                                                    </div>
                                                    <!--end::Tabpane-->
    
                                                    <!--begin::Tabpane-->
                                                    <div class="tab-pane" id="topbar_notifications_teams" role="tabpanel">
                                                        <!--begin::Nav-->
                                                        <div class="d-flex flex-center text-center min-h-200px">
                                                            All caught up!
                                                            <br/>
                                                            No new notifications.
                                                        </div>
                                                        <!--end::Nav-->
                                                    </div>
                                                    <!--end::Tabpane-->
                                                </div>
                                                <!--end::Content-->
                                            </form>
                                        </div>
                                        <!--end::Dropdown-->
                                    </div>
                                    <!--end::Notifications-->

                                    <!--begin::Chat-->
                                    <div class="topbar-item mr-2">
                                        <div class="btn btn-icon btn-lg" data-toggle="modal" data-target="#Sb_chat_modal">
                                        <span class="svg-icon svg-icon-xl">
                                            <i class="fas fa-comments"></i>
                                        </span>	 			   
                                        </div>
                                    </div>
                                    <!--end::Chat-->

    
                                    <!--begin::User-->
                                    <div class="topbar-item">
                                        <div class="btn btn-icon w-lg-auto d-flex align-items-center btn-lg px-2" id="Sb_quick_user_toggle">
                                            <div class="d-flex flex-column text-right pr-lg-3">
                                                <span class="font-weight-bold font-size-sm d-none d-md-inline">Chanchal</span>
                                                <span class="font-weight-bolder font-size-sm d-none d-md-inline">UX Designer</span>
                                            </div>
                                            <span class="symbol symbol-35">
                                                <span class="symbol-label font-size-h5 font-weight-bold ">CS</span>
                                            </span>
                                        </div>
                                    </div>
                                    <!--end::User-->
                                </div>
                                <!--end::Topbar-->
                            </div>
                            <!--end::Container-->
                        </div>
                        <!--end::Top-->
    
                        <!--begin::Bottom-->
                        <div class="header-bottom">
                            <!--begin::Container-->
                            <div class=" container-fluid ">
                                <!--begin::Header Menu Wrapper-->
                                <div class="header-navs header-navs-left" id="Sb_header_navs">
                                    <!--begin::Tab Navs(for tablet and mobile modes)-->
                                    <ul class="header-tabs p-5 p-lg-0 d-flex d-lg-none nav nav-bold nav-tabs" role="tablist">
                                        <!--begin::Item-->
                                        <li class="nav-item mr-2">
                                            <a href="#" class="nav-link btn btn-clean active" data-toggle="tab" data-target="#Sb_header_dashboard" role="tab">
                                                Home
                                            </a>
                                        </li>
                                        <!--end::Item-->
    
                                        <!--begin::Item-->
                                        <li class="nav-item mr-2">
                                            <a href="#" class="nav-link btn btn-clean" data-toggle="tab" data-target="#Sb_header_discovery" role="tab">
                                                Discovery
                                            </a>
                                        </li>
                                        <!--end::Item-->
                                        
                                        <!--begin::Item-->
                                        <li class="nav-item mr-2">
                                            <a href="#" class="nav-link btn btn-clean" data-toggle="tab" data-target="#Sb_header_reports" role="tab">
                                                Reports
                                            </a>
                                        </li>
                                        <!--end::Item-->

                                        <!--begin::Item-->
                                        <li class="nav-item mr-3">
                                            <a href="#" class="nav-link py-4 px-6" data-toggle="tab" data-target="#Sb_header_boards" role="tab">
                                                Boards
                                            </a>
                                        </li>
                                        <!--end::Item-->

                                        <!--begin::Item-->
                                        <li class="nav-item mr-3">
                                            <a href="#" class="nav-link py-4 px-6" data-toggle="tab" data-target="#Sb_header_library" role="tab">
                                                Image Library
                                            </a>
                                        </li>
                                        <!--end::Item-->
                                        
                                        <!--begin::Item-->
                                        <li class="nav-item mr-3">
                                            <a href="#" class="nav-link py-4 px-6" data-toggle="tab" data-target="#Sb_header_chat" role="tab">
                                                Chat
                                            </a>
                                        </li>
                                        <!--end::Item-->

                                    </ul>
                                    <!--begin::Tab Navs-->
    
                                    <!--begin::Tab Content-->
                                    <div class="tab-content">
                                        <!--begin::Dashboard Tab Pane-->
                                        <div class="tab-pane py-5 p-lg-0 show active" id="Sb_header_dashboard">
                                            <!--begin::Menu-->
                                            <div id="Sb_header_menu" class="header-menu header-menu-mobile  header-menu-layout-default " >
                                                <!--begin::Nav-->
                                                <ul class="menu-nav ">
                                                    <li class="menu-item "  aria-haspopup="true">
                                                        <a  href="dashboard.html" class="menu-link btn font-weight-bold my-2 my-lg-0 mr-3">
                                                            <span class="menu-text"><i class="fas fa-tachometer-alt fa-fw"></i> Dashboard</span>
                                                        </a>
                                                    </li>
                                                    <li class="menu-item menu-item-submenu menu-item-rel"  data-menu-toggle="click" aria-haspopup="true">
                                                        <a  href="javascript:;" class="menu-link menu-toggle btn font-weight-bold my-2 my-lg-0 mr-3">
                                                            <span class="menu-text"><i class="fas fa-film fa-fw"></i> Publishing</span>
                                                            <span class="menu-desc"></span><i class="menu-arrow"></i>
                                                        </a>
                                                        <div class="menu-submenu menu-submenu-classic menu-submenu-left" >
                                                            <ul class="menu-subnav">
                                                                <li class="menu-item  menu-item-submenu"  data-menu-toggle="hover" aria-haspopup="true">
                                                                    <a  href="publishing/schedule_messages.html" class="menu-link">
                                                                        <span class="menu-text">Schedule Messages</span>
                                                                    </a>
                                                                </li>
                                                                <li class="menu-item  menu-item-submenu"  data-menu-toggle="hover" aria-haspopup="true">
                                                                    <a  href="publishing/socioqueue.html" class="menu-link">
                                                                        <span class="menu-text">SocioQueue</span>
                                                                    </a>
                                                                </li>
                                                                <li class="menu-item  menu-item-submenu"  data-menu-toggle="hover" aria-haspopup="true">
                                                                    <a  href="publishing/day_wise_socioqueue.html" class="menu-link">
                                                                        <span class="menu-text">Day Wise SocioQueue</span>
                                                                    </a>
                                                                </li>
                                                                <li class="menu-item  menu-item-submenu"  data-menu-toggle="hover" aria-haspopup="true">
                                                                    <a  href="publishing/drafts.html" class="menu-link">
                                                                        <span class="menu-text">Drafts</span>
                                                                    </a>
                                                                </li>
                                                                <li class="menu-item  menu-item-submenu"  data-menu-toggle="hover" aria-haspopup="true">
                                                                    <a  href="publishing/calendar_view.html" class="menu-link">
                                                                        <span class="menu-text">Calendar View</span>
                                                                    </a>
                                                                </li>
                                                                <li class="menu-item  menu-item-submenu"  data-menu-toggle="hover" aria-haspopup="true">
                                                                    <a  href="publishing/history.html" class="menu-link">
                                                                        <span class="menu-text">History</span>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li class="menu-item  menu-item-submenu menu-item-rel"  data-menu-toggle="click" aria-haspopup="true">
                                                        <a  href="javascript:;" class="menu-link menu-toggle btn font-weight-bold my-2 my-lg-0 mr-3">
                                                            <span class="menu-text"><i class="fas fa-people-arrows fa-fw"></i> Engagement</span>
                                                            <span class="menu-desc"></span><i class="menu-arrow"></i>
                                                        </a>
                                                        <div class="menu-submenu menu-submenu-classic menu-submenu-left" >
                                                            <ul class="menu-subnav">
                                                                <li class="menu-item  menu-item-submenu"  data-menu-toggle="hover" aria-haspopup="true">
                                                                    <a  href="engagement/twt_analytics.html" class="menu-link">
                                                                        <span class="menu-text">Twitter Analytics</span>
                                                                    </a>
                                                                </li>
                                                                <li class="menu-item  menu-item-submenu"  data-menu-toggle="hover" aria-haspopup="true">
                                                                    <a  href="engagement/twt_inbox.html" class="menu-link">
                                                                        <span class="menu-text">Twitter Inbox</span>
                                                                    </a>
                                                                </li>
                                                                <li class="menu-item  menu-item-submenu"  data-menu-toggle="hover" aria-haspopup="true">
                                                                    <a  href="engagement/my_task.html" class="menu-link">
                                                                        <span class="menu-text">My Task</span>
                                                                    </a>
                                                                </li>
                                                                <li class="menu-item  menu-item-submenu"  data-menu-toggle="hover" aria-haspopup="true">
                                                                    <a  href="engagement/yt_inbox.html" class="menu-link">
                                                                        <span class="menu-text">Youtube Inbox</span>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                </ul>        
                                                <!--end::Nav-->
                                            </div>
                                            <!--end::Menu-->
                                        </div>
                                        <!--begin::Dashboard Tab Pane-->

                                        <!--begin::Discovery Tab Pane-->
                                        <div class="tab-pane py-5 p-lg-0 justify-content-between" id="Sb_header_discovery">
                                            <!--begin::Menu-->
                                            <div id="Sb_header_menu_1" class="header-menu header-menu-mobile  header-menu-layout-default" >
                                                <!--begin::Nav-->
                                                <ul class="menu-nav">
                                                    <li class="menu-item  menu-item-submenu menu-item-rel"  data-menu-toggle="click" aria-haspopup="true">
                                                        <a  href="javascript:;" class="menu-link menu-toggle btn font-weight-bold my-2 my-lg-0 mr-3">
                                                            <span class="menu-text"><i class="fas fa-search fa-fw"></i> Discovery</span>
                                                            <span class="menu-desc"></span><i class="menu-arrow"></i>
                                                        </a>
                                                        <div class="menu-submenu menu-submenu-classic menu-submenu-left" >
                                                            <ul class="menu-subnav">
                                                                <li class="menu-item  menu-item-submenu"  data-menu-toggle="hover" aria-haspopup="true">
                                                                    <a  href="javascript:;" class="menu-link menu-toggle"><i class="menu-bullet menu-bullet-dot"><span></span></i>
                                                                        <span class="menu-text">Search</span><i class="menu-arrow"></i>
                                                                    </a>
                                                                    <div class="menu-submenu menu-submenu-classic menu-submenu-right" >
                                                                        <ul class="menu-subnav">
                                                                            <li class="menu-item" aria-haspopup="true">
                                                                                <a  href="discovery/search_d.html" class="menu-link "><i class="menu-bullet menu-bullet-dot"><span></span></i>
                                                                                    <span class="menu-text">Discovery</span></a>
                                                                            </li>
                                                                            <li class="menu-item" aria-haspopup="true">
                                                                                <a  href="discovery/search_s.html" class="menu-link "><i class="menu-bullet menu-bullet-dot"><span></span></i>
                                                                                    <span class="menu-text">Smart Search</span></a>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </li>
                                                                <li class="menu-item  menu-item-submenu"  data-menu-toggle="hover" aria-haspopup="true">
                                                                    <a  href="javascript:;" class="menu-link menu-toggle">
                                                                        <i class="menu-bullet menu-bullet-dot"><span></span></i>
                                                                        <span class="menu-text">Group Post</span><i class="menu-arrow"></i>
                                                                    </a>
                                                                    <div class="menu-submenu menu-submenu-classic menu-submenu-right" >
                                                                        <ul class="menu-subnav">
                                                                            <li class="menu-item" aria-haspopup="true">
                                                                                <a  href="discovery/group_fb.html" class="menu-link ">
                                                                                    <i class="menu-bullet menu-bullet-dot"><span></span></i>
                                                                                    <span class="menu-text">Facebook Group Post</span>
                                                                                </a>
                                                                            </li>
                                                                            <li class="menu-item" aria-haspopup="true">
                                                                                <a  href="discovery/group_in.html" class="menu-link ">
                                                                                    <i class="menu-bullet menu-bullet-dot"><span></span></i>
                                                                                    <span class="menu-text">Linkedin Group Post</span>
                                                                                </a>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li class="menu-item  menu-item-submenu menu-item-rel"  data-menu-toggle="click" aria-haspopup="true">
                                                        <a  href="javascript:;" class="menu-link menu-toggle btn font-weight-bold my-2 my-lg-0 mr-3">
                                                            <span class="menu-text"><i class="fas fa-images fa-fw"></i> Feeds</span>
                                                            <span class="menu-desc"></span><i class="menu-arrow"></i>
                                                        </a>
                                                        <div class="menu-submenu menu-submenu-classic menu-submenu-left" >
                                                            <ul class="menu-subnav">
                                                                <li class="menu-item  menu-item-submenu"  data-menu-toggle="hover" aria-haspopup="true">
                                                                    <a  href="feeds/facebook.html" class="menu-link">
                                                                        <span class="menu-text">Facebook</span>
                                                                    </a>
                                                                </li>
                                                                <li class="menu-item  menu-item-submenu"  data-menu-toggle="hover" aria-haspopup="true">
                                                                    <a  href="feeds/instagram.html" class="menu-link">
                                                                        <span class="menu-text">Instagram</span>
                                                                    </a>
                                                                </li>
                                                                <li class="menu-item  menu-item-submenu"  data-menu-toggle="hover" aria-haspopup="true">
                                                                    <a  href="feeds/twitter.html" class="menu-link">
                                                                        <span class="menu-text">Twitter</span>
                                                                    </a>
                                                                </li>
                                                                <li class="menu-item  menu-item-submenu"  data-menu-toggle="hover" aria-haspopup="true">
                                                                    <a  href="feeds/youtube.html" class="menu-link">
                                                                        <span class="menu-text">Youtube</span>
                                                                    </a>
                                                                </li>
                                                                <li class="menu-item  menu-item-submenu"  data-menu-toggle="hover" aria-haspopup="true">
                                                                    <a  href="feeds/linkedin.html" class="menu-link">
                                                                        <span class="menu-text">Linkedin</span>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li class="menu-item  menu-item-submenu menu-item-rel"  data-menu-toggle="click" aria-haspopup="true">
                                                        <a  href="javascript:;" class="menu-link menu-toggle btn font-weight-bold my-2 my-lg-0 mr-3">
                                                            <span class="menu-text"><i class="far fa-newspaper fa-fw"></i> Content Studio</span>
                                                            <span class="menu-desc"></span><i class="menu-arrow"></i>
                                                        </a>
                                                        <div class="menu-submenu menu-submenu-classic menu-submenu-left" >
                                                            <ul class="menu-subnav">
                                                                <li class="menu-item  menu-item-submenu"  data-menu-toggle="hover" aria-haspopup="true">
                                                                    <a  href="content_studio/most_shared.html" class="menu-link">
                                                                        <span class="menu-text">Most Shared</span>
                                                                    </a>
                                                                </li>
                                                                <li class="menu-item  menu-item-submenu"  data-menu-toggle="hover" aria-haspopup="true">
                                                                    <a  href="content_studio/trending_now.html" class="menu-link">
                                                                        <span class="menu-text">Trending Now</span>
                                                                    </a>
                                                                </li>
                                                                <li class="menu-item  menu-item-submenu"  data-menu-toggle="hover" aria-haspopup="true">
                                                                    <a  href="content_studio/shareathon_queue.html" class="menu-link">
                                                                        <span class="menu-text">Shareathon Queue</span>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li class="menu-item  menu-item-submenu menu-item-rel"  data-menu-toggle="click" aria-haspopup="true">
                                                        <a  href="javascript:;" class="menu-link menu-toggle btn font-weight-bold my-2 my-lg-0 mr-3">
                                                            <span class="menu-text"><i class="fas fa-rss fa-fw"></i> Automated RSS feeds</span>
                                                            <span class="menu-desc"></span><i class="menu-arrow"></i>
                                                        </a>
                                                        <div class="menu-submenu menu-submenu-classic menu-submenu-left" >
                                                            <ul class="menu-subnav">
                                                                <li class="menu-item  menu-item-submenu"  data-menu-toggle="hover" aria-haspopup="true">
                                                                    <a  href="rss/content.html" class="menu-link">
                                                                        <span class="menu-text">Content Feeds</span>
                                                                    </a>
                                                                </li>
                                                                <li class="menu-item  menu-item-submenu"  data-menu-toggle="hover" aria-haspopup="true">
                                                                    <a  href="rss/automated.html" class="menu-link">
                                                                        <span class="menu-text">Automated RSS feeds</span>
                                                                    </a>
                                                                </li>
                                                                <li class="menu-item  menu-item-submenu"  data-menu-toggle="hover" aria-haspopup="true">
                                                                    <a  href="rss/posted.html" class="menu-link">
                                                                        <span class="menu-text">Posted RSS feeds</span>
                                                                    </a>
                                                                </li>
                                                                <li class="menu-item  menu-item-submenu"  data-menu-toggle="hover" aria-haspopup="true">
                                                                    <a  href="rss/feeds.html" class="menu-link">
                                                                        <span class="menu-text">RSS feeds</span>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li class="menu-item  menu-item-submenu menu-item-rel"  data-menu-toggle="click" aria-haspopup="true">
                                                        <a  href="javascript:;" class="menu-link menu-toggle btn font-weight-bold my-2 my-lg-0 mr-3">
                                                            <span class="menu-text"><i class="fas fa-share-alt-square fa-fw"></i> Shareathon</span>
                                                            <span class="menu-desc"></span><i class="menu-arrow"></i>
                                                        </a>
                                                        <div class="menu-submenu menu-submenu-classic menu-submenu-left" >
                                                            <ul class="menu-subnav">
                                                                <li class="menu-item  menu-item-submenu"  data-menu-toggle="hover" aria-haspopup="true">
                                                                    <a  href="shareathon/page.html" class="menu-link">
                                                                        <span class="menu-text">Page Shareathon</span>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                </ul>        
                                                <!--end::Nav-->
                                            </div>
                                            <!--end::Menu-->
                                        </div>
                                        <!--begin::Discovery Tab Pane-->
    
                                        <!--begin::Reports Tab Pane-->
                                        <div class="tab-pane p-5 p-lg-0 justify-content-between" id="Sb_header_reports">
                                            <div class="d-flex flex-column flex-lg-row align-items-start align-items-lg-center">
                                                <!--begin::Actions-->
                                                <a href="reports/archived_report.html" class="btn font-weight-bold mr-3 my-2 my-lg-0">
                                                    <i class="fas fa-clipboard-check fa-fw"></i> Archived Report
                                                </a>
    
                                                <a href="reports/primitive_report.html" class="btn font-weight-bold mr-3 my-2 my-lg-0">
                                                    <i class="fas fa-notes-medical fa-fw"></i> Primitive Report
                                                </a>

                                                <a href="reports/team_report.html" class="btn font-weight-bold mr-3 my-2 my-lg-0">
                                                    <i class="fas fa-id-badge fa-fw"></i> Team Report
                                                </a>
                                                <!--end::Actions-->
                                            </div>
                                        </div>
                                        <!--begin::Reports Tab Pane-->

                                        <!--begin::Boards Tab Pane-->
                                        <div class="tab-pane p-5 p-lg-0 justify-content-between" id="Sb_header_boards">
                                            <div class="d-flex flex-column flex-lg-row align-items-start align-items-lg-center">
                                                <!--begin::Actions-->
                                                <a href="boardme/view_boards.html" class="btn font-weight-bold mr-3 my-2 my-lg-0">
                                                    <i class="fas fa-chalkboard fa-fw"></i> View Boards
                                                </a>
    
                                                <a href="boardme/create_board.html" class="btn font-weight-bold mr-3 my-2 my-lg-0">
                                                    <i class="fas fa-chalkboard-teacher fa-fw"></i> Create Board
                                                </a>
                                                <!--end::Actions-->
                                            </div>
    
                                        </div>
                                        <!--begin::Boards Tab Pane-->

                                        <!--begin::Image Library Tab Pane-->
                                        <div class="tab-pane p-5 p-lg-0 justify-content-between" id="Sb_header_library">
                                            <div class="d-flex flex-column flex-lg-row align-items-start align-items-lg-center">
                                                <!--begin::Actions-->
                                                <a href="library/public.html" class="btn font-weight-bold mr-3 my-2 my-lg-0">
                                                    <i class="fas fa-eye fa-fw"></i> Public
                                                </a>
    
                                                <a href="library/private.html" class="btn font-weight-bold mr-3 my-2 my-lg-0">
                                                    <i class="fas fa-eye-slash fa-fw"></i> Private
                                                </a>
                                                <!--end::Actions-->
                                            </div>
    
                                        </div>
                                        <!--begin::Image Library Tab Pane-->
                                        
                                        <!--begin::Chat Tab Pane-->
                                        <div class="tab-pane p-5 p-lg-0 justify-content-between" id="Sb_header_chat">
                                            <div class="d-flex flex-column flex-lg-row align-items-start align-items-lg-center">
                                                <!--begin::Actions-->
                                                <a href="chat/group_chat.html" class="btn font-weight-bold mr-3 my-2 my-lg-0">
                                                    <i class="fas fa-comments fa-fw"></i> Group Chat
                                                </a>
                                                <!--end::Actions-->
                                            </div>

                                        </div>
                                        <!--begin::Chat Tab Pane-->

                                    </div>
                                    <!--end::Tab Content-->
                                </div>
                                <!--end::Header Menu Wrapper-->
                            </div>
                            <!--end::Container-->
                        </div>
                        <!--end::Bottom-->
                    </div>
                <!--end::Header-->
        {{-- Begin content for login --}}
        @yield('content')

        <!--begin::Global Theme Bundle(used by all pages)-->
        <script src="{{asset('plugins/global/plugins.bundle.js')}}"></script>
        <script src="{{asset('plugins/custom/prismjs/prismjs.bundle.js')}}"></script>
        <script src="{{asset('js/main.js')}}"></script>
        <script src="{{asset('js/custom.js')}}"></script>
        <!--end::Global Theme Bundle-->

        {{-- scripts that are used in blades --}}
        @yield('scripts')
        {{-- end scripts that are used in blades --}}


    </body>
    <!--end::Body-->
</html>