<?php

use Illuminate\Support\Facades\Route;
use Modules\Home\Http\Controllers\DashboardController;


Route::group(['module' => 'Home', 'middleware' => ['web'], 'namespace' => 'App\Modules\Home\Controllers'], function() {

    // show user dashboard page
    Route::get('/dashboard', [DashboardController::class, 'index'])
        ->name('dashboard')
        ->middleware('role:user');

    // show admin page
    Route::get('/admin', [DashboardController::class, 'index'])
        ->name('admin')
        ->middleware('role:admin');

});