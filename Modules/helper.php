<?php

namespace Modules;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Http;

class Helper{

    protected $client;
    protected $token;
    private static $_instance = null;

    public function __construct()
    {
        $this->API_URL = env('API_URL') . env('VERSION') . '/';
    }

    public function ApiLoginCall($data, $route){
        $response = Http::post($this->API_URL . $route, [
            'email' => $data->email,
            'password' => $data->password,
        ]);
        return $response;
    }

    public function ApiRegisterCall($data, $route){
        $response = Http::post($this->API_URL . $route, [
            'email' => $data->email,
            'password' => $data->password,
            'name' => $data->name,
        ]);
        return $response;
    }
}

?>